const websocket_port = 8000
const nodejs_port = 8081
const express = require('express')
const app = express()
const server = require('http').Server(app)
server.listen(websocket_port)
const io = require('socket.io')(server)
const path = require('path')
const canvas = require('canvas')
const cron = require('node-cron')
const sharedsession = require('express-socket.io-session')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const fs = require('fs')
const mysql = require('./db.js')
const faceapi = require('./src/face-api.min.js')
const bcrypt = require('bcrypt')
const crypto = require('crypto')
const fetch = require('node-fetch')
faceapi.env.monkeyPatch({ fetch: fetch })
const Canvas = canvas
const Image = canvas
const ImageData = canvas
faceapi.env.monkeyPatch({ Canvas, Image, ImageData })
let faceMatcher
app.use(cookieParser())
let safe_username = new RegExp('^[a-zA-Z0-9]{2,32}$')

/*
* Change the secret.
*/
const session = require('express-session')({
    secret: 'my-secret',
    resave: true,
    saveUninitialized: true
})

app.use(session)

io.use(sharedsession(session, {
    autoSave: true
}))

/*
* Change this key. Use 256-bit key. This is used for encrypting and decrypting the facial descriptor data.
*/
const key = 'S0mIWfmUpyJNjlQNLCbWJ5Vyhuc7htwg'

/*
* In the client-side we are saving 5 facial descriptors by default (can be changed via global const descriptors_needed in the client.js).
* In the server-side when we are comparing the descriptors, we need at least 3 matches to successfully log in the user.
* If we save 10 descriptors in the client-side, it would be a good idea to change the matches_we_need to 6 or something.
* The 0.6 threshold refers to the euclidean distance of the descriptors. 0.6 is the default value for the face-api.js.
* You can play around with these values.
*/
const matches_we_need = 3
const distance_threshold = 0.6
                            
/*
* Because of the demo purposes, we want the database and the facial descriptor files to be deleted every 24 hours.
*/
cron.schedule('0 0 * * *', () => {
    mysql.query('TRUNCATE TABLE users', true, (err, result, fields) => {
        if(err) {
            console.log('error while trying to truncate the users table')
        }
    })
    let dir = __dirname + '/face_descriptors'
    fs.readdir(dir, (err, files) => {
        if (err) {
            console.log('error while trying to delete the facial descriptor files in the /face_descriptors directory',  err)
        }
        for (const file of files) {
            fs.unlink(path.join(dir, file), err => {
                if (err) {
                    console.log('error while trying to delete a facial descriptor file in the /face_descriptors directory', err)
                }
            })
        }
    })
})

let modelsPath = __dirname + '/src/models'
Promise.all([
    faceapi.nets.faceRecognitionNet.loadFromDisk(modelsPath),
    faceapi.nets.faceLandmark68Net.loadFromDisk(modelsPath),
    faceapi.nets.faceExpressionNet.loadFromDisk(modelsPath),
    faceapi.nets.tinyFaceDetector.loadFromDisk(modelsPath),
    faceapi.nets.ageGenderNet.loadFromDisk(modelsPath)
]).then(() => {
    console.log('loaded all the models from the disk')
})

function encrypt(text) {
    const iv = crypto.randomBytes(16)
    let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv)
    let encrypted = cipher.update(text)
    encrypted = Buffer.concat([encrypted, cipher.final()])
    return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') }
}

function decrypt(text) {
    let iv = Buffer.from(text.iv, 'hex')
    let encryptedText = Buffer.from(text.encryptedData, 'hex')
    let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv)
    let decrypted = decipher.update(encryptedText)
    decrypted = Buffer.concat([decrypted, decipher.final()])
    return decrypted.toString()
}

io.on('connection', (socket) => {
    console.log('user connected with a session: ', socket.handshake.session)
    let logged_in = false
    let session_data = socket.handshake.session.data
    if(session_data) {
        if(session_data.username) {
            logged_in = true
            socket.emit('login', { success: true, reauthed: true })
        } else {
            socket.emit('login', { success: false })
        }
    }

    socket.on('login', (client_data) => {
        /*
        * Login is done in a two step process.
        * First we do the normal login, by checking if the credentials are correct.
        * If the credentials are correct, we start the facial recognition.
        * So the client sends: 
        *   socket.emit('login', { username: 'bob', password: 'secretpassword' })
        * And the client receives (if the credentials are correct):
        *   { success: 'waiting', reason: 'RECOGNITION_STARTED' }
        * Client then starts the facial recognition, and sends the facial descriptors to the server.
        * Server then loads a file from the disk which has the reference data for
        * the client's facial descriptors (saved during registeration) and compares it to the the
        * facial descriptors we got from the client during logging in.
        */
        if(socket.handshake.session.data) {
            if(socket.handshake.session.data.username) {
                logged_in = true
            }
        }
        if(!logged_in) {
            if(!client_data.facialdata) {
                let username = client_data.username
                let password = client_data.password
                if(safe_username.test(username)) {
                    mysql.query('SELECT username, password FROM users WHERE username = ?', [username], (err, result, fields) => {
                        if (err) {
                            console.log(err)
                        } else {
                            if(result.length > 0) {
                                if(bcrypt.compareSync(password, result[0].password)) {
                                    socket.emit('login', { success: 'waiting', reason: 'RECOGNITION_STARTED' })
                                    socket.handshake.session.data = { temp_username: username }
                                    socket.handshake.session.save()
                                } else {
                                    socket.emit('login', { success: false, reason: 'INVALID_PASSWORD' })
                                }
                            } else {
                                socket.emit('login', { success: false, reason: 'INVALID_PASSWORD' })
                            }
                        }
                    })
                } else {
                    socket.emit('login', { success: false, reason: 'VALIDATION_ERROR' })
                }
            } else {
                if(socket.handshake.session.data.temp_username) {
                    filePath = __dirname + '/face_descriptors/data_' + socket.handshake.session.data.temp_username + '.txt'
                    fs.readFile(filePath, { encoding: 'utf-8' }, async (error, reference_data) => {
                        if(!error) {
                            /*
                            * Since the LabeledFaceDescriptors expects descriptors to be an array of Float32Array,
                            * we have to create the array from the data we got from the client.
                            * After we have the array ready, we create a FaceMatcher from it.
                            * Then we have to create a similar array from the saved reference data we have on the disk.
                            * After we have that array ready, we start the facial recognition by comparing
                            * euclidean distances by using the faceMatcher.findBestMatch().
                            */
                            let ref_data = JSON.parse(reference_data)
                            const decrypted_facialdata = decrypt(ref_data)
                            ref_data = JSON.parse(decrypted_facialdata)

                            const ref_descs_len = ref_data.length
                            const decs_vector_len = Object.keys(ref_data[0].descriptor).length
                            let descs = []
                            var helper_arr = []

                            for(var i = 0; i < ref_descs_len; i++) {
                                for(var j = 0; j < decs_vector_len; j++) {
                                    helper_arr.push(ref_data[i].descriptor[j])
                                }
                                let f32_arr = new Float32Array(helper_arr)
                                descs.push(f32_arr)
                                helper_arr = []
                            }

                            const labeledFaceDescriptors = new faceapi.LabeledFaceDescriptors('person', descs)
                            faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors)

                            const client_descs_len = client_data.facialdata.length
                            const client_descs_vector_len = Object.keys(client_data.facialdata[0].descriptor).length
                            const client_decs = []
                            var helper_arr = []
 
                            for(var i = 0; i < client_descs_len; i++) {
                                for(var j = 0; j < client_descs_vector_len; j++) {
                                    helper_arr.push(client_data.facialdata[i].descriptor[j])
                                }
                                let f32_arr = new Float32Array(helper_arr)
                                client_decs.push(f32_arr)
                                helper_arr = []
                            }

                            let enough_matches = false
                            let matches_count = 0

                            for(var i = 0; i < client_decs.length; i++){
                                if(matches_count >= matches_we_need) {
                                    break
                                }
                                const bestMatch = faceMatcher.findBestMatch(client_decs[i])
                                if(bestMatch._distance <= distance_threshold) {
                                    matches_count++
                                }
                            }

                            if(matches_count >= matches_we_need) {
                                socket.handshake.session.data = { username: socket.handshake.session.data.temp_username }
                                delete socket.handshake.session.data.temp_username
                                socket.handshake.session.save()
                                socket.emit('login', { success: true, username: socket.handshake.session.data.username })
                            } else {
                                socket.emit('login', { success: false, reason: 'FACES_DONT_MATCH' })
                            }
                        } else {
                            socket.emit('login', { success: false, reason: 'ERROR_READING_REFERENCE_FACIALDATA' })
                            console.log(error)
                        }
                    })
                } else {
                    socket.emit('login', { success: false, reason: 'NOT_LOGGED_IN' })
                }
            }
        } else {
            socket.emit('login', { success: false, reason: 'ALREADY_LOGGED' })
        }
    })

    socket.on('register', (data) => {
        let hashed_pw = bcrypt.hashSync(data.password, 10)
        let username = data.username
        if(safe_username.test(username)) {
            mysql.query('INSERT INTO users (username, password) VALUES (?, ?)', [username, hashed_pw], (err, result, fields) => {
                if (err) {
                    socket.emit('register', { success: false, reason: 'USERNAME_ALREADY_EXISTS' })
                } else {
                    let facialdata = encrypt(JSON.stringify(data.facialdata))
                    let writer = fs.createWriteStream(__dirname + '/face_descriptors/data_' + username + '.txt')
                    writer.write(JSON.stringify(facialdata))
                    writer.on('error', function (err) {
                        console.log(err)
                        socket.emit('register', { success: false, reason: 'FAILED_TO_WRITE_TO_DISK' })
                    })
                    /* Uncomment these lines if you want to automatically log the user in after the registeration is complete. */
                    //socket.handshake.session.data = { username: username }
                    //socket.handshake.session.save()
                    socket.emit('register', { success: true, username: username })
                }
            })
        } else {
            socket.emit('register', { success: false, reason: 'VALIDATION_ERROR' })
        }
    })

    socket.on('logout', () => {
        if (socket.handshake.session.data) {
            socket.emit('logout', { result: true })
            logged_in = false
            delete socket.handshake.session.data
            socket.handshake.session.save()
        } else {
            socket.emit('logout', { result: false })
        }
    })
})

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'src/index.html'))
})

app.use(bodyParser.json())
app.use(express.static(__dirname + '/src'))
app.listen(nodejs_port, () => console.log('Node.js server running on port: ' + nodejs_port))
