# Facelogin
[https://facelogin.vaa.red](https://facelogin.vaa.red)
User authentication by facial recognition using JavaScript.

When you register, your facial landmarks will be saved in the database under your username. When you log in, your facial landmarks must match what's in the database. Your password and facial landmarks are encrypted.

Facial recognition is done by using the [face-api.js]([https://github.com/justadudewhohacks/face-api.js](https://github.com/justadudewhohacks/face-api.js)) library.

**Note**: All of the accounts and the data associated to them will be erased every 24 hours.

## Under the hood
* [Node.js](https://nodejs.org/en)
* Socket.IO
* All of the facial recognition is done by the face-api.js library
* mysql

## Known issues
* It's slow/laggy. Mainly because we are demoing the facial recognition by showing the landmarks, expressions etc. to the user while registering and logging in. You can simply disable this by commenting the `drawOnCanvas` on the `src/client.js`.
* Some browsers (like Brave) or extensions block the facial recognition.
