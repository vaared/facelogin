'use strict'
let mysql = require('mysql')

/*
You want to have a "users" table in your db like so:
    CREATE TABLE `users` (
      `username` VARCHAR(255) NOT NULL,
      `password` VARCHAR(255) NOT NULL,
      PRIMARY KEY (`username`)
    );
*/

let connection = mysql.createConnection({
    host     : process.env.RDS_HOSTNAME,
    user     : process.env.RDS_USERNAME,
    password : process.env.RDS_PASSWORD,
    port     : process.env.RDS_PORT,
    database : process.env.RDS_DB_NAME
})

connection.connect((err) => {
    if (err) {
        throw err
    }
})

module.exports = connection;
